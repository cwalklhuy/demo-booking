$(document).ready(function () {
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/bookings",
        dataType: "json",
        success: function (data) {
            let response = JSON.parse(JSON.stringify(data));
            let bookings = response.object;
            for (let i = 0; i < bookings.length; i++) {
                let booking = {
                    username: bookings[i].username,
                    bookingId: bookings[i].bookingId,
                    name: bookings[i].name,
                    startTime: bookings[i].startTime,
                    endTime: bookings[i].endTime,
                    dateCreated: bookings[i].dateCreated,
                    status: bookings[i].status
                };
                //<th scope="row">1</th>
                let row1 = '<th scope="row">' + booking.username + '</th>';
                let row2 = '<td>' + booking.startTime + ' - ' + booking.endTime + '</td>';
                let row3 = '<td>' + booking.dateCreated + '</td>';
                let row4 = '<td>' + booking.status + '</td>';
                let markup = '<tr>' + row1 + row2 + row3 + row4 + '</tr>';
                $('.table tbody').append(markup);
            }
        },
        error: function (e) {
            console.log("Error: ", e);
        }
    })
})