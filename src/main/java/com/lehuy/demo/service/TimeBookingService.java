package com.lehuy.demo.service;

import java.util.List;

import com.lehuy.demo.entity.TimeBooking;

public interface TimeBookingService {

	String save(List<TimeBooking> timeBookings);

	TimeBooking getTimeBookingById(Long timeBookingId);

}
