package com.lehuy.demo.service;

import java.time.LocalDate;
import java.util.Collection;

import org.springframework.stereotype.Service;

import com.lehuy.demo.entity.Booking;
import com.lehuy.demo.entity.TimeBooking;
import com.lehuy.demo.repository.BookingRepository;

@Service
public class BookingServiceImpl implements BookingService{

	private final BookingRepository bookingRepository;

	public BookingServiceImpl(BookingRepository bookingRepository) {
		this.bookingRepository = bookingRepository;
	}

	@Override
	public Booking saveBooking(Booking booking) {

		return bookingRepository.save(booking);
	}

	@Override
	public Booking checkDateAndTimeBooking(TimeBooking timeBooking, LocalDate now) {

		return bookingRepository.findBookingsByTimeBookingAndDateCreated(timeBooking, now);
	}

	@Override
	public Collection<Booking> getBookingsByStatusNotEqualConfirmed() {

		return bookingRepository.findAllBookingByStatusNotConfirmed();
	}

	@Override
	public Collection<Booking> getBookingsByUsernameAndStatusNotConfirmed(String username) {

		return bookingRepository.findAllByUsernameAndStatusNotConfirmed(username);
	}

	@Override
	public Collection<Booking> getAllBookings() {

		return bookingRepository.findAll();
	}

}
