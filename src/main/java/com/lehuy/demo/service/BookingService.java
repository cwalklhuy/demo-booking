package com.lehuy.demo.service;

import java.time.LocalDate;
import java.util.Collection;

import com.lehuy.demo.entity.Booking;
import com.lehuy.demo.entity.TimeBooking;

public interface BookingService {

	Booking saveBooking(Booking booking);

	Booking checkDateAndTimeBooking(TimeBooking timeBooking, LocalDate now);

	Collection<Booking> getBookingsByStatusNotEqualConfirmed();

	Collection<Booking> getBookingsByUsernameAndStatusNotConfirmed(String username);

	Collection<Booking> getAllBookings();
}
