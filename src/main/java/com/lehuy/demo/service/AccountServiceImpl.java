package com.lehuy.demo.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lehuy.demo.entity.Account;
import com.lehuy.demo.repository.AccountRepository;

@Service
public class AccountServiceImpl implements AccountService{

	private final AccountRepository accountRepository;

	public AccountServiceImpl(AccountRepository accountRepository) {
		this.accountRepository = accountRepository;
	}

	@Override
	public Account getAccountByUsername(String username) {
		Optional<Account> account = accountRepository.findById(username);

		return account.isPresent() ? account.get() : null;
	}

}
