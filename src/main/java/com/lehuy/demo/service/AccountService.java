package com.lehuy.demo.service;

import com.lehuy.demo.entity.Account;

public interface AccountService {
	
	Account getAccountByUsername(String username);
}
