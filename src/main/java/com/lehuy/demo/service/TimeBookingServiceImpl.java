package com.lehuy.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.lehuy.demo.entity.TimeBooking;
import com.lehuy.demo.repository.TimeBookingRepository;

@Service
public class TimeBookingServiceImpl implements TimeBookingService{

	private final TimeBookingRepository timeBookingRepository;

	public TimeBookingServiceImpl(TimeBookingRepository timeBookingRepository) {
		this.timeBookingRepository = timeBookingRepository;
	}

	@Override
	public String save(List<TimeBooking> timeBookings) {
		List<TimeBooking> list = timeBookingRepository.saveAll(timeBookings);
		
		return list.isEmpty() ? "Failed" : "Success";
	}

	@Override
	public TimeBooking getTimeBookingById(Long timeBookingId) {
		Optional<TimeBooking> timeBooking = timeBookingRepository.findById(timeBookingId);
		
		return timeBooking.orElse(null);
	}

}
