package com.lehuy.demo.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.lehuy.demo.entity.json.BookingForExpert;
import org.springframework.web.bind.annotation.*;

import com.lehuy.demo.entity.Account;
import com.lehuy.demo.entity.Booking;
import com.lehuy.demo.entity.TimeBooking;
import com.lehuy.demo.entity.json.BookingJson;
import com.lehuy.demo.response.ResponseModel;
import com.lehuy.demo.service.AccountService;
import com.lehuy.demo.service.BookingService;
import com.lehuy.demo.service.TimeBookingService;

@RestController
@RequestMapping("/bookings")
public class BookingController {
	
	private final AccountService accountService;
	
	private final TimeBookingService timeBookingService;
	
	private final BookingService bookingService;

	public BookingController(AccountService accountService, TimeBookingService timeBookingService, BookingService bookingService) {
		this.accountService = accountService;
		this.timeBookingService = timeBookingService;
		this.bookingService = bookingService;
	}

	@PostMapping("")
	public ResponseModel saveBooking(@RequestBody BookingJson bookingJson) {
		Account account = accountService.getAccountByUsername(bookingJson.getUsername());
		TimeBooking timeBooking = timeBookingService.getTimeBookingById(bookingJson.getTimeBookingId());
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate localDate = LocalDate.parse(bookingJson.getDateCreated(),formatter);
		ResponseModel res = new ResponseModel();
		if (account != null && timeBooking != null) {
			Booking checkBooking = bookingService.checkDateAndTimeBooking(timeBooking, localDate);
			if (checkBooking != null) {
				res.setMessage("Booking has already. Please choose other time");
				res.setStatus("FAILED");
			} else {
				res.setMessage("Booking successful");
				res.setStatus("SUCCESS");

				Booking booking = Booking.builder()
						.account(account)
						.timeBooking(timeBooking)
						.dateCreated(localDate)
						.status("confirming")
						.build();
				res.setObject(bookingService.saveBooking(booking));
			}
		} else {
			res.setStatus("SOMETHING WENT WRONG");
			res.setMessage("Please check Server");
		}
		
		return res;
	}

	@GetMapping("")
	public ResponseModel getBookings() {
		final Collection<Booking> bookings = bookingService.getBookingsByStatusNotEqualConfirmed();

		return getResponseModel(bookings);
	}

	@GetMapping("/{username}")
	public ResponseModel getBookingByUserId(@PathVariable String username) {
		Collection<Booking> bookings = bookingService.getBookingsByUsernameAndStatusNotConfirmed(username);

		return getResponseModel(bookings);
	}

	@GetMapping("/booking-expert")
	public ResponseModel getAllBookingForExpert() {
		Collection<Booking> bookings = bookingService.getAllBookings();

		return getResponseModel(bookings);
	}

	private ResponseModel getResponseModel(Collection<Booking> bookings) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		ResponseModel res = new ResponseModel();
		List<BookingForExpert> result = new ArrayList<>();
		bookings.forEach(e -> result.add(BookingForExpert.builder()
				.username(e.getAccount().getUsername())
				.bookingId(e.getId())
				.startTime(e.getTimeBooking().getStartTime())
				.endTime(e.getTimeBooking().getEndTime())
				.name(e.getAccount().getName())
				.dateCreated(formatter.format(e.getDateCreated()))
				.status(e.getStatus())
				.build()));
		if (bookings.isEmpty()) {
			res.setMessage("The booking have not today");
			res.setStatus("FAILED");
		} else {
			res.setMessage("List booking");
			res.setStatus("SUCCESS");
			res.setObject(result);
		}

		return res;
	}
}
