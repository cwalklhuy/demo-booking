package com.lehuy.demo.controller;

import java.util.Collections;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lehuy.demo.entity.TimeBooking;
import com.lehuy.demo.service.TimeBookingService;


@RestController
@RequestMapping("/timebookings")
public class TimeBookingController {

	private final TimeBookingService timeBookingService;

	public TimeBookingController(TimeBookingService timeBookingService) {
		this.timeBookingService = timeBookingService;
	}

	@GetMapping("/save")
	public String sayHello() {
		TimeBooking time3 = TimeBooking.builder()
				.startTime("17:00")
				.endTime("19:00")
				.build();
		List<TimeBooking> timeBookings = Collections.singletonList(time3);

		return timeBookingService.save(timeBookings);
	}
}
