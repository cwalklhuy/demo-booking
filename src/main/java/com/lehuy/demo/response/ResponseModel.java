package com.lehuy.demo.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class ResponseModel {

	private String status;
	private String message;
	private Object object;

	public ResponseModel() {
		this.status = "";
		this.message = "";
		this.object = null;
	}
}
