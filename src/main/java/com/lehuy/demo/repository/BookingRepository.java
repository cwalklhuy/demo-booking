package com.lehuy.demo.repository;

import com.lehuy.demo.entity.TimeBooking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.lehuy.demo.entity.Booking;

import java.time.LocalDate;
import java.util.Collection;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Long>{

    Booking findBookingsByTimeBookingAndDateCreated(TimeBooking timeBooking, LocalDate now);

    @Query("SELECT b from Booking b where b.status <> 'confirmed'")
    Collection<Booking> findAllBookingByStatusNotConfirmed();

    @Query("select b from Booking b where b.account.username = ?1 and b.status <> 'confirmed'")
    Collection<Booking> findAllByUsernameAndStatusNotConfirmed(String username);
}
