package com.lehuy.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lehuy.demo.entity.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, String>{

}
