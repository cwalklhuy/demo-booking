package com.lehuy.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lehuy.demo.entity.TimeBooking;

@Repository
public interface TimeBookingRepository extends JpaRepository<TimeBooking, Long>{

}
