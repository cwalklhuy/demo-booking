package com.lehuy.demo.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
@Entity
@Table(name = "booking")
public class Booking implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
	private Long id;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE})
    @JoinColumn(name = "username_id", referencedColumnName = "username")
    private Account account;

    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE})
    @JoinColumn(name = "time_booking_id", referencedColumnName = "id")
    private TimeBooking timeBooking;
    
    @Column(name = "date_created")
    private LocalDate dateCreated;

    @Column(name = "status")
    private String status;

	public Booking() {
		this.id = Long.valueOf("0");
		this.account = null;
		this.timeBooking = null;
		this.dateCreated = null;
	}
    
    
}
