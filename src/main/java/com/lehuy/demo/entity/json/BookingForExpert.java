package com.lehuy.demo.entity.json;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class BookingForExpert {
    private String username;
    private Long bookingId;
    private String name;
    private String startTime;
    private String endTime;
    private String dateCreated;
    private String status;

}
