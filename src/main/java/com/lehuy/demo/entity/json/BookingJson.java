package com.lehuy.demo.entity.json;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class BookingJson {
	private String username;
	private Long timeBookingId;
	private String dateCreated;
}
